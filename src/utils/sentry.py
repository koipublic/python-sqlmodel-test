# builtin imports
import os
import sys
import traceback

import toml
from sentry_sdk import capture_exception, capture_message
from sentry_sdk import init as sentry_init
from sentry_sdk import set_context

from ._logger import logger


class SentrySetup:
    def __init__(self) -> None:
        with open("pyproject.toml", "r") as file:
            toml_data = toml.loads(file.read())
            os.environ["SENTRY_RELEASE"] = toml_data["tool"]["poetry"]["version"]

        sentry_init(
            dsn=os.getenv("SENTRY_DSN", None),
            max_breadcrumbs=90,
            debug=False,
            attach_stacktrace=True,
        )

    @staticmethod
    def setContext(context_name: str, data: dict) -> None:
        set_context(context_name, data)

    @staticmethod
    def capExc(err: Exception):
        capture_exception(err)
        logger.error(err)
        if os.getenv("SENTRY_ENVIRONMENT", "").upper() != "PRODUCTION":
            traceback.print_exc()

    @staticmethod
    def capMsg(msg: str) -> None:
        logger.info(f"Sentry capmsg : {msg}")
        capture_message(msg)


sentry_kr = SentrySetup()
