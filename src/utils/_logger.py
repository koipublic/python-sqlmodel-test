# builtin imports
import os
import sys

# pypi imports
from loguru import logger

import src.utils.settings

MODULE_NAME = "REGION_DETECTION_PIPELINE"


def set_logger(is_enable=True, log_level=os.getenv("LOG_LEVEL", "WARNING")):
    """
    is_enable: boolean, default - False
    log_level: takes in all loguru log levels, will
        look for LOG_LEVEL environment variable else set to be default WARNING
    """
    if is_enable:
        logger.remove()
        logger.add(sys.stdout, level=log_level)
        logger.enable(MODULE_NAME)
    else:
        logger.disable(MODULE_NAME)


if os.environ.get("PY_ENV", "").upper() == "PRODUCTION":
    set_logger()
else:
    set_logger(log_level="DEBUG")
